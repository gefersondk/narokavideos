package adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import model.MGallery;
import model.NKvideos;
import pb.st.br.mago7.narokavideos.R;
import utility.CircleImageLoadProgressBar;

/**
 * Created by Geferson on 10/09/16.
 */
public class AdapterGallery extends RecyclerView.Adapter<AdapterGallery.ViewHolderNK>{

    private List<MGallery> mGallery;
    private Context context;
    private Activity activity;
    private boolean animation;
    private LayoutInflater inflater;

    public AdapterGallery(Context context, List<MGallery> nKvideosList, Activity activity, boolean animation) {
        this.context = context;
        this.mGallery = nKvideosList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        this.animation = animation;
    }

    @Override
    public AdapterGallery.ViewHolderNK onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_gallery, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolderNK viewHolderNK = new ViewHolderNK(view);
        return viewHolderNK;
    }

    @Override
    public void onBindViewHolder(AdapterGallery.ViewHolderNK holder, int position) {

        String img[] = new String[]{"https://s-media-cache-ak0.pinimg.com/736x/69/74/95/697495b8c2e11b5d247e8a554c5f313a.jpg","http://2.bp.blogspot.com/-UhmoWydEghk/T0VdzGbXsMI/AAAAAAAACx8/pkm2oPzz-VA/s1600/BUNDA+GOSTOSA.jpg","https://passoka.files.wordpress.com/2012/10/mulher-gostosa1.jpg","https://cratonoticias.files.wordpress.com/2013/03/gostosas-da-net1576bcd346e3143c9e157273bb268505c1.jpg","http://www.ultracurioso.com.br/wp-content/uploads/2015/10/Gostosa-600x341.png","http://cdn.inquisitr.com/wp-content/uploads/2015/09/mia-khalifa-instagram-hacked.jpg"};

        //String img = "http://lorempixel.com/400/200/";
        Random random = new Random();
        int p = random.nextInt(img.length);
        //holder.photo.setImageURI(Uri.parse(""));
        DraweeController controllerf = Fresco.newDraweeControllerBuilder()
                .setUri(img[p])
                .setTapToRetryEnabled(true)
                .setOldController(holder.photo.getController())
                .setAutoPlayAnimations(true).build();
        holder.photo.getHierarchy().setProgressBarImage(new CircleImageLoadProgressBar());//
        holder.photo.setController(controllerf);

        if(animation){
            try{
                YoYo.with(Techniques.ZoomIn)
                        .duration(700)
                        .playOn(holder.container_card);
            }catch(Exception e){
                e.printStackTrace();
            }
        }


    }

    @Override
    public int getItemCount() {
        return this.mGallery.size();
    }

    public void addListItem(MGallery dataObject, int position) {
        mGallery.add(dataObject);
        notifyItemInserted(position);
    }

    public void removeListItem(int position) {
        mGallery.remove(position);
        notifyItemRemoved(position);
    }

    public void addAll(List<MGallery> mkGallery){
        this.mGallery.clear();
        this.mGallery.addAll(mkGallery);
        notifyDataSetChanged();
    }

    public List<MGallery> getNKvideosList(){
        return this.mGallery;
    }

    public void cleanAll() {
        this.mGallery.clear();
        notifyDataSetChanged();
    }

    public static class ViewHolderNK extends RecyclerView.ViewHolder {


        @BindView(R.id.img_gallery)
        protected SimpleDraweeView photo;

        @BindView(R.id.card_gallery)
        protected CardView card_gallery;

        //@BindView(R.id.container_card)
        protected LinearLayout container_card;

        public ViewHolderNK(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

