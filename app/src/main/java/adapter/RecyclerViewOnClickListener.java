package adapter;

import android.view.View;

/**
 * Created by Geferson on 22/08/16.
 */
public interface RecyclerViewOnClickListener {

    public void onClickListener(View view, int position);
    public void onLongPressClickListener(View view, int position);

}