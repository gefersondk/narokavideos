package adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import model.NKvideos;
import pb.st.br.mago7.narokavideos.R;
import utility.CircleImageLoadProgressBar;

/**
 * Created by Geferson on 23/08/16.
 */
public class AdapterHistoric extends RecyclerView.Adapter<AdapterHistoric.ViewHolderNK>{

    private List<NKvideos> videosList;
    private Context context;
    private Activity activity;
    private boolean animation;
    private LayoutInflater inflater;

    public AdapterHistoric(Context context, List<NKvideos> nKvideosList, Activity activity, boolean animation) {
        this.context = context;
        this.videosList = nKvideosList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        this.animation = animation;
    }

    @Override
    public AdapterHistoric.ViewHolderNK onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_historic, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolderNK viewHolderNK = new ViewHolderNK(view);
        return viewHolderNK;
    }

    @Override
    public void onBindViewHolder(AdapterHistoric.ViewHolderNK holder, int position) {

        String img[] = new String[]{"https://s-media-cache-ak0.pinimg.com/736x/69/74/95/697495b8c2e11b5d247e8a554c5f313a.jpg","http://2.bp.blogspot.com/-UhmoWydEghk/T0VdzGbXsMI/AAAAAAAACx8/pkm2oPzz-VA/s1600/BUNDA+GOSTOSA.jpg","https://passoka.files.wordpress.com/2012/10/mulher-gostosa1.jpg","https://cratonoticias.files.wordpress.com/2013/03/gostosas-da-net1576bcd346e3143c9e157273bb268505c1.jpg","http://www.ultracurioso.com.br/wp-content/uploads/2015/10/Gostosa-600x341.png","http://cdn.inquisitr.com/wp-content/uploads/2015/09/mia-khalifa-instagram-hacked.jpg"};

        //String img = "http://lorempixel.com/400/200/";
        Random random = new Random();
        int p = random.nextInt(img.length);
        //holder.photo.setImageURI(Uri.parse(""));
        DraweeController controllerf = Fresco.newDraweeControllerBuilder()
                .setUri(img[p])
                .setTapToRetryEnabled(true)
                .setOldController(holder.photo.getController())
                .setAutoPlayAnimations(true).build();
        holder.photo.getHierarchy().setProgressBarImage(new CircleImageLoadProgressBar());//
        holder.photo.setController(controllerf);

        /*holder.total.setText(String.format("R$ %s",String.valueOf(this.vdfList.get(position).getTotal())));

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();


        for (OrderItem o: vdfList.get(position).getOrderItem()) {
            entries.add(new PieEntry(o.getQuantidade(), o.getProduct().getName()));

        }*/
    }

    @Override
    public int getItemCount() {
        return this.videosList.size();
    }

    public void addListItem(NKvideos dataObject, int position) {
        videosList.add(dataObject);
        notifyItemInserted(position);
    }

    public void removeListItem(int position) {
        videosList.remove(position);
        notifyItemRemoved(position);
    }

    public void addAll(List<NKvideos> nKvideos){
        this.videosList.clear();
        this.videosList.addAll(nKvideos);
        notifyDataSetChanged();
    }

    public List<NKvideos> getNKvideosList(){
        return this.videosList;
    }

    public void cleanAll() {
        this.videosList.clear();
        notifyDataSetChanged();
    }

    public static class ViewHolderNK extends RecyclerView.ViewHolder {

        //@Bind(R.id.chart)
        //protected PieChart pieChart;
        //@Bind(R.id.date_order)
        //protected TextView date;
        //@Bind(R.id.total_order_list)
        //protected TextView total;
        //@Bind(R.id.order_id)
        //protected TextView order_id;
        @BindView(R.id.img_video)
        protected SimpleDraweeView photo;

        public ViewHolderNK(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            //photo = (SimpleDraweeView) itemView.findViewById(R.id.img_home_news);
        }
    }
}
