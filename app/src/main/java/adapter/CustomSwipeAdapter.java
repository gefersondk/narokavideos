package adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.IOException;

import butterknife.BindView;
import pb.st.br.mago7.narokavideos.R;
import utility.CircleImageLoadProgressBar;

/**
 * Created by Geferson on 21/08/16.
 */
public class CustomSwipeAdapter extends PagerAdapter{

    private Context context;
    private String [] imgs;
    private LayoutInflater layoutInflater;
    private Activity activity;

    //@BindView(R.id.video_view)
    //protected VideoView videoView;
    String pk;
    private MediaPlayer mMediaPlayer;
    String urlVideo = "https://www.youtube.com/watch?v=aJT9F2oHrSg";
    String uri;
    public CustomSwipeAdapter(Context context, Activity activity, String []imgs){
        this.context = context;
        this.imgs = imgs;
        uri = "android.resource://" + context.getPackageName() + "/" + R.raw.videoplayback;
        //textureView.setSurfaceTextureListener(getA);
        this.activity =activity;

        //videoView.setVideoPath();

    }
    @Override
    public int getCount() {
        return 3;//this.imgs.length
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (LinearLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View item_view;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        /*if(position == 0){
            item_view = layoutInflater.inflate(R.layout.swipe_layout_txt_view, container, false);
            //VideoView videoView = (VideoView) item_view.findViewById(R.id.video_view);
            //videoView.setVideoURI(Uri.parse(uri));
            //videoView.start();
            //emVideoView = (EMVideoView) item_view.findViewById(R.id.video_view2);
            //emVideoView.setOnPreparedListener(this);
            //emVideoView.setVideoURI(Uri.parse("https://archive.org/download/Popeye_forPresident/Popeye_forPresident_512kb.mp4"));
            //emVideoView.start();


            container.addView(item_view);
        }else{
            item_view = layoutInflater.inflate(R.layout.swipe_layout, container, false);
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) item_view.findViewById(R.id.img_swipe);
            DraweeController controllerf = Fresco.newDraweeControllerBuilder()
                    .setUri(imgs[position])
                    .setTapToRetryEnabled(true)
                    .setOldController(simpleDraweeView.getController())
                    .setAutoPlayAnimations(true).build();
            simpleDraweeView.getHierarchy().setProgressBarImage(new CircleImageLoadProgressBar());//
            simpleDraweeView.setController(controllerf);
            container.addView(item_view);
        }*/
        item_view = layoutInflater.inflate(R.layout.swipe_layout, container, false);
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) item_view.findViewById(R.id.img_swipe);
        DraweeController controllerf = Fresco.newDraweeControllerBuilder()
                .setUri(imgs[position])
                .setTapToRetryEnabled(true)
                .setOldController(simpleDraweeView.getController())
                .setAutoPlayAnimations(true).build();
        simpleDraweeView.getHierarchy().setProgressBarImage(new CircleImageLoadProgressBar());//
        simpleDraweeView.setController(controllerf);
        container.addView(item_view);
        return  item_view;
        //return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((LinearLayout) object);
    }

}
