package adapter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import model.News;
import pb.st.br.mago7.narokavideos.R;

/**
 * Created by Geferson on 22/08/16.
 */
public class AdapterHome extends RecyclerView.Adapter<AdapterHome.ViewHolder>{

    private Context context;
    private List<News> newsList;
    private Activity activity;
    private LayoutInflater inflater;
    private ProgressBarDrawable progressBarDrawable;

    public AdapterHome(Context context, List<News> newsList, Activity activity){
        this.context = context;
        this.newsList = newsList;
        this.activity = activity;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.progressBarDrawable = new ProgressBarDrawable();
        //this.progressBarDrawable.setBackgroundColor(R.color.red);
        //this.progressBarDrawable.setAlpha(R.color.red);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_news, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolder myViewHolder =  new ViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {



        if(position%2 == 0){

            //holder.photo.setImageURI(Uri.parse(""));
            DraweeController controllerf = Fresco.newDraweeControllerBuilder()
                    .setUri("http://videoviews.international/wp-content/uploads/2016/02/buy-vimeo-views.jpg")
                    .setTapToRetryEnabled(true)
                    .setOldController(holder.photo.getController())
                    .setAutoPlayAnimations(true).build();
            holder.photo.getHierarchy().setProgressBarImage(new ProgressBarDrawable());//
            holder.photo.setController(controllerf);
        }else{
            //holder.photo.setImageURI(Uri.parse("https://lh3.googleusercontent.com/qzeDHEH4itIbIKtU0XAr-VMB7bKYCG4wsgWoJno2PaHuTxzMAPaWwPHMdvewrwgNcfq3Pdq8pg=s630-fcrop64=1,00410000fef7ffff"));
            DraweeController controllerf = Fresco.newDraweeControllerBuilder()
                    .setUri("https://lh3.googleusercontent.com/qzeDHEH4itIbIKtU0XAr-VMB7bKYCG4wsgWoJno2PaHuTxzMAPaWwPHMdvewrwgNcfq3Pdq8pg=s630-fcrop64=1,00410000fef7ffff")
                    .setTapToRetryEnabled(true)
                    .setOldController(holder.photo.getController())
                    .setAutoPlayAnimations(true).build();
            holder.photo.getHierarchy().setProgressBarImage(this.progressBarDrawable);//
            holder.photo.setController(controllerf);
        }

    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public void addListItem(News tickte, int position){
        newsList.add(tickte);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        newsList.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected SimpleDraweeView photo;

        public ViewHolder(View itemView) {
            super(itemView);

            photo = (SimpleDraweeView) itemView.findViewById(R.id.img_home_news);

        }
    }
}