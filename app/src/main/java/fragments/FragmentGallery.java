package fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.ChangeBounds;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import adapter.AdapterGallery;
import adapter.RecyclerViewOnClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.MGallery;
import model.NKvideos;
import pb.st.br.mago7.narokavideos.ActivityAddLib;
import pb.st.br.mago7.narokavideos.R;

/**
 * Created by Geferson on 10/09/16.
 */
public class FragmentGallery extends Fragment implements RecyclerViewOnClickListener{

    private Context context;
    private List<MGallery> mGalleryList;
    private AdapterGallery adapterGallery;
    //private RecyclerView.LayoutManager layoutManager;
    private GridLayoutManager gridLayoutManager;
    @BindView(R.id.recyclerView_gallery)
    protected RecyclerView recyclerView;

    @BindView(R.id.fab_add_gallery)
    protected FloatingActionButton fab;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            Explode explode = new Explode();
            explode.setDuration(100);
            Fade fade = new Fade();
            fade.setDuration(100);


            //this.setEnterTransition(explode);
            //this.setExitTransition(new ChangeBounds());
            //this.setReturnTransition(fade);


            //this.setExitTransition(explode);
            //this.setReenterTransition(fade);

            /*TransitionInflater inflater = TransitionInflater.from(getActivity());
            Transition transition = inflater.inflateTransition(R.transition.transitions);
            //getActivity().getWindow().setSharedElementExitTransition(transition);
            this.setExitTransition(transition);*/
            //this.setSharedElement
        }
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);
        this.context = getActivity().getApplicationContext();
        //layoutManager = new LinearLayoutManager(getActivity());

        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), recyclerView, (RecyclerViewOnClickListener) this));
        //recyclerView.setNestedScrollingEnabled(false);

        mGalleryList = new ArrayList<>();

        for(int i = 0; i< 51; i++){
            mGalleryList.add(new MGallery());
        }

        adapterGallery = new AdapterGallery(context, mGalleryList, getActivity(), false);
        recyclerView.setAdapter(adapterGallery);
        adapterGallery.notifyDataSetChanged();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){

                    View view1 = view;

                    //ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), Pair.create(view1, "element1")); //n tem elementos compartilhados coloca null
                    //ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), null); //n tem elementos compartilhados coloca null

                   // getActivity().startActivity(new Intent(getActivity(), ActivityAddLib.class), activityOptionsCompat.toBundle());
                    getActivity().startActivity(new Intent(getActivity(), ActivityAddLib.class));
                }else{
                    getActivity().startActivity(new Intent(getActivity(), ActivityAddLib.class));
                }
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        int display_mode = getResources().getConfiguration().orientation;

        if (display_mode == Configuration.ORIENTATION_PORTRAIT) {
            gridLayoutManager = new GridLayoutManager(getActivity(), 2);
            recyclerView.setLayoutManager(gridLayoutManager);

        } else {
            gridLayoutManager = new GridLayoutManager(getActivity(), 3);
            recyclerView.setLayoutManager(gridLayoutManager);

        }
    }

    @Override
    public void onClickListener(View view, int position) {

    }

    @Override
    public void onLongPressClickListener(View view, int position) {

    }

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{
        private Context context;
        private GestureDetector gestureDetector;
        RecyclerViewOnClickListener recyclerViewOnClickListener;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListener rVOnCLContact){
            this.context = c;
            this.recyclerViewOnClickListener = rVOnCLContact;
            this.gestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListener != null){
                        recyclerViewOnClickListener.onLongPressClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListener != null){
                        recyclerViewOnClickListener.onClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }

                    return true;
                }
            });

        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            //Toast.makeText(context, "onInterceptTouchEvent",Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            //Toast.makeText(context, "onTouchEvent",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            //Toast.makeText(context, "onRequestDisallowInterceptTouchEvent",Toast.LENGTH_SHORT).show();
        }

    }


}
