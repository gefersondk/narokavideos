package fragments;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import adapter.AdapterHome;
import adapter.CustomSwipeAdapter;
import adapter.RecyclerViewOnClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.News;
import pb.st.br.mago7.narokavideos.MainActivity;
import pb.st.br.mago7.narokavideos.R;

/**
 * Created by Geferson on 21/08/16.
 */
public class FragmentHome extends Fragment implements RecyclerViewOnClickListener {

    private Context context;
    private CustomSwipeAdapter adapter;
    @BindView(R.id.pager)
    protected ViewPager viewPager;
    //@BindView(R.id.radiogroup)
    //protected RadioGroup radioGroup;
    @BindView(R.id.id_home_news_recycler_view)
    protected RecyclerView tRecyclerView;
    private AdapterHome adapterHome;
    private RecyclerView.LayoutManager layoutManager;
    private List<News> newsList;

    private ImageView[] dots;
    private int dotsCount;

    @BindView(R.id.container_pager_indicator)
    protected LinearLayout containerPagerIndicator;

    protected BottomBar bottomBar;

    private MainActivity act;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.newsList = getSetHomeNews(9);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        this.context = getActivity().getApplicationContext();
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        tRecyclerView.setHasFixedSize(true);

        String packageName = getActivity().getPackageName();

        adapter = new CustomSwipeAdapter(context, getActivity(),  new String[]{"http://nofilmschool.com/sites/default/files/styles/article_wide/public/201503077_2_img_fix_700x700.jpg?itok=6v1mX5nY","http://www.maispb.com.br/wp-content/uploads/2016/06/N%C3%BAbia.jpg","http://imguol.com/c/bol/fotos/3b/2016/05/07/7mai2016---parece-que-acabou-a-fase-comportada-da-modelo-russa-anastasiya-kvitko-ha-dias-ela-publicou-imagens-sensuais-porem-bem-mais-discretas-do-que-esta-habituada-veja-fotos-anteriores-mas-1462636104047_595x444.jpg"});
        viewPager.setAdapter(adapter);

        layoutManager = new LinearLayoutManager(getActivity());
        //tLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        tRecyclerView.setLayoutManager(layoutManager);
        tRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), tRecyclerView, (RecyclerViewOnClickListener) this));
        //tRecyclerView.setNestedScrollingEnabled(false);



        //bottomBar = (BottomBar) view.findViewById(R.id.bottomBar);

        /*act = (MainActivity) getActivity();


        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.home) {
                    // The tab with id R.id.tab_favorites was selected,
                    // change your content accordingly.
                    //if (act instanceof MainActivity)
                        //act.replaceFragment("home");
                }
                if (tabId == R.id.historic){
                    //if (act instanceof MainActivity)
                        //act.replaceFragment("historic");
                }
            }
        });*/



        for(int i = 0; i< 11; i++){
            newsList.add(new News());
        }

        adapterHome = new AdapterHome(getActivity().getApplicationContext(), this.newsList, getActivity());

        tRecyclerView.setAdapter(adapterHome);
        //adapterHome.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dots = new ImageView[adapter.getCount()];
        dotsCount = adapter.getCount();

        for (int i = 0; i < dotsCount; i ++){

            dots[i] = new ImageView(context);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );


            params.setMargins(4, 0, 4, 0);
            //else
                //params.setMargins(4, 0, 4, 0);
            containerPagerIndicator.addView(dots[i], params);


            if(i == 0){
                dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_item_radio));
            }


        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem));
                }

                dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_item_radio));
                /*switch (position) {
                    case 0:
                        radioGroup.check(R.id.radioButton);
                        break;
                    case 1:
                        radioGroup.check(R.id.radioButton2);
                        break;
                    case 2:
                        radioGroup.check(R.id.radioButton3);
                        break;
                }*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private List<News> getSetHomeNews(int qnt){
        List<News> newsListAux = new ArrayList<News>();
        for(int i = 0; i< qnt; i++){
            newsListAux.add(new News());
        }
        return newsListAux;
    }

    @Override
    public void onClickListener(View view, int position) {
        //Toast.makeText(getActivity(), "click", Toast.LENGTH_SHORT).show();
//        Bundle bundle = new Bundle();
//        bundle.putInt("position", position);
//        startActivity(new Intent(getActivity(), ShowItemQrcode.class).putExtras(bundle));
//        AdapterHome adapterRecyclerview = (AdapterHome) tRecyclerView.getAdapter();
    }

    @Override
    public void onLongPressClickListener(View view, int position) {
        //Toast.makeText(getActivity(), "Long click", Toast.LENGTH_SHORT).show();
    }

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{
        private Context context;
        private GestureDetector gestureDetector;
        RecyclerViewOnClickListener recyclerViewOnClickListener;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListener rVOnCLContact){
            this.context = c;
            this.recyclerViewOnClickListener = rVOnCLContact;
            this.gestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListener != null){
                        recyclerViewOnClickListener.onLongPressClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListener != null){
                        recyclerViewOnClickListener.onClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }

                    return true;
                }
            });

        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            //Toast.makeText(context, "onInterceptTouchEvent",Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            //Toast.makeText(context, "onTouchEvent",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            //Toast.makeText(context, "onRequestDisallowInterceptTouchEvent",Toast.LENGTH_SHORT).show();
        }

    }
}
