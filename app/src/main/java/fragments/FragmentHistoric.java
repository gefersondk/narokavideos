package fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import adapter.AdapterHistoric;
import adapter.AdapterHome;
import adapter.RecyclerViewOnClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.NKvideos;
import model.News;
import pb.st.br.mago7.narokavideos.ActivityDisplayNKvideo;
import pb.st.br.mago7.narokavideos.R;

/**
 * Created by Geferson on 23/08/16.
 */
public class FragmentHistoric extends Fragment implements RecyclerViewOnClickListener {

    private Context context;
    private List<NKvideos> videosList;
    private AdapterHistoric adapterHistoric;
    private RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.recyclerView_historic)
    protected RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_historic, container, false);
        ButterKnife.bind(this, view);
        this.context = getActivity().getApplicationContext();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), recyclerView, (RecyclerViewOnClickListener) this));

        videosList = new ArrayList<>();

        for(int i = 0; i< 11; i++){
            videosList.add(new NKvideos());
        }

        adapterHistoric = new AdapterHistoric(context, videosList, getActivity(), false);
        //recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapterHistoric);
        adapterHistoric.notifyDataSetChanged();

        return view;
    }

    private List<NKvideos> getSetHistoric(int qnt){
        List<NKvideos> nKvideos = new ArrayList<NKvideos>();
        for(int i = 0; i< qnt; i++){
            nKvideos.add(new NKvideos());
        }
        return nKvideos;
    }

    @Override
    public void onClickListener(View view, int position) {
        startActivity(new Intent(context, ActivityDisplayNKvideo.class));
    }

    @Override
    public void onLongPressClickListener(View view, int position) {

    }

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{
        private Context context;
        private GestureDetector gestureDetector;
        RecyclerViewOnClickListener recyclerViewOnClickListener;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListener rVOnCLContact){
            this.context = c;
            this.recyclerViewOnClickListener = rVOnCLContact;
            this.gestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListener != null){
                        recyclerViewOnClickListener.onLongPressClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListener != null){
                        recyclerViewOnClickListener.onClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }

                    return true;
                }
            });

        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            //Toast.makeText(context, "onInterceptTouchEvent",Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            //Toast.makeText(context, "onTouchEvent",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            //Toast.makeText(context, "onRequestDisallowInterceptTouchEvent",Toast.LENGTH_SHORT).show();
        }

    }
}
