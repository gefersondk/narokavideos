package fragments;

import android.support.v4.app.Fragment;

/**
 * Created by Geferson on 21/08/16.
 */
public enum FragmentSw {
    home {
        @Override
        public Fragment getNKfragment() {
            return new FragmentHome();
        }
    },
    gallery {
        @Override
        public Fragment getNKfragment() {
            return new FragmentGallery();
        }
    },
    historic {
        @Override
        public Fragment getNKfragment() {
            return new FragmentHistoric();
        }
    };
    public abstract Fragment getNKfragment();
}