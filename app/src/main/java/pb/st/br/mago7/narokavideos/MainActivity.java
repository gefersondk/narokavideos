package pb.st.br.mago7.narokavideos;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.Build;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import fragments.FragmentHome;
import fragments.FragmentSw;

public class MainActivity extends AppCompatActivity{

    public static final String KEY_LOCAL_FILE = BuildConfig.APPLICATION_ID + "KEY_LOCAL_FILE";
    //private FensterVideoView textureView;
    //private SimpleMediaFensterPlayerController playerController;
    private FragmentManager fragmentManager;
    private FragmentSw sw;
    private Fragment fragment;

    //@BindView(R.id.toolbar)
    //protected Toolbar toolbar;
    protected BottomBar bottomBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            Explode explode = new Explode();
            explode.setDuration(3000);
            Fade fade = new Fade();
            fade.setDuration(3000);

            //getWindow().setEnterTransition(explode);
            //getWindow().setExitTransition(explode);
            //getWindow().setReenterTransition(fade);
        }

        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        this.fragmentManager = getSupportFragmentManager();
        if(savedInstanceState == null){
            this.fragment = new FragmentHome();
            fragmentManager.beginTransaction().replace(R.id.contentContainer,fragment).commit();
        }


        bindViews();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);

    }



    private void bindViews() {

        bottomBar = (BottomBar) findViewById(R.id.bottomBar);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.home) {
                    // The tab with id R.id.tab_favorites was selected,
                    // change your content accordingly.
                    replaceFragment("home");
                }
                if(tabId == R.id.gallery){
                    replaceFragment("gallery");
                    //bottomBar.se
                }
                if (tabId == R.id.historic){
                    replaceFragment("historic");
                }
            }
        });


    }

    public  void replaceFragment(String tag){
        sw = FragmentSw.valueOf(tag);
        fragment = sw.getNKfragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        //fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.contentContainer, fragment);
        fragmentTransaction.commit();
    }

}
