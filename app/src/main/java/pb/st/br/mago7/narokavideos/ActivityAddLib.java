package pb.st.br.mago7.narokavideos;

import android.app.Activity;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Animatable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.widget.VideoView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import utility.CircleImageLoadProgressBar;

/**
 * Created by Geferson on 11/09/16.
 */
public class ActivityAddLib extends Activity implements TextureView.SurfaceTextureListener {

    //@BindView(R.id.img_add_gallery_load)
    //protected SimpleDraweeView simpleDraweeView;

    //@BindView(R.id.videoView)
    //protected VideoView videoView;

    private static final String FILE_NAME = "videoplayback.mp4";

    @BindView(R.id.textureView1)
    protected TextureView textureView;
    private MediaPlayer mMediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("chamou","onCreate");
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            Explode explode = new Explode();
            explode.setDuration(200);
            Fade fade = new Fade();
            fade.setDuration(200);


            getWindow().setEnterTransition(explode);
            getWindow().setReturnTransition(fade);
            /*TransitionInflater inflater = TransitionInflater.from(this);
            Transition transition = inflater.inflateTransition(R.transition.transitions);
            getWindow().setSharedElementEnterTransition(new Explode());//new ChangeBounds()
            getWindow().setSharedElementExitTransition(new Slide());*/
        }

        setContentView(R.layout.activity_add_lib);//http://taihinhnendong.com/hinh-nen/module_new/hinh-dong-tuyet-roi_s897.gif
        super.onCreate(savedInstanceState); //http://attachments.gfan.com/forum/attachments2/day_120828/120828080052afe8bd2720ccb6.gif
        ButterKnife.bind(this);

        ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(
                    String id,
                    @Nullable ImageInfo imageInfo,
                    @Nullable Animatable anim) {
                if (anim != null) {
                    // app-specific logic to enable animation starting
                    anim.start();
                }
            }
        };
        /*xxxhdpi: 1280x1920 px
        xxhdpi: 960x1600 px
        xhdpi: 640x960 px
        hdpi: 480x800 px
        mdpi: 320x480 px
        ldpi: 240x320 px*/

        String url = "http://attachments.gfan.com/forum/attachments2/day_120828/120828080052afe8bd2720ccb6.gif";//http://66.media.tumblr.com/ffbf998af88fffe0f12c58f4864a7e43/tumblr_o62qlj5Ulx1qbg3s6o1_1280.png
        DraweeController controllerf = Fresco.newDraweeControllerBuilder()
                .setUri(url)
                .setAutoPlayAnimations(true)
                .setControllerListener(controllerListener)
                .setTapToRetryEnabled(true).build();
                //.setOldController(simpleDraweeView.getController()).build();
        //simpleDraweeView.getHierarchy().setProgressBarImage(new CircleImageLoadProgressBar());//
        //simpleDraweeView.setController(controllerf);

        /*String urlVideo = "https://www.youtube.com/watch?v=aJT9F2oHrSg";
        String uri = "android.resource://" + getPackageName() + "/" + R.raw.videoplayback;
        //videoView.setVideoPath();
        videoView.setVideoURI(Uri.parse(uri));
        videoView.start();*/

        //textureView = new TextureView(this);
        textureView.setSurfaceTextureListener(this);
        //setContentView(textureView);




    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Surface s = new Surface(surface);

        try {

            String uri = "android.resource://" + getPackageName() + "/" + R.raw.videoplayback;

            mMediaPlayer= new MediaPlayer();
            //mMediaPlayer.setDataSource(uri);//"https://thumbs.dreamstime.com/videothumb_large7558/75587820.mp4"
            mMediaPlayer.setDataSource(getApplicationContext(), Uri.parse(uri));
            mMediaPlayer.setSurface(s);
            mMediaPlayer.setLooping(true);
            //mMediaPlayer.prepare();
            //mMediaPlayer.setOnBufferingUpdateListener(this);
            //mMediaPlayer.setOnCompletionListener(this);
            //mMediaPlayer.setOnPreparedListener(this);
            //mMediaPlayer.setOnVideoSizeChangedListener(this);


            //mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            //mMediaPlayer.start();

            mMediaPlayer.prepareAsync();

            // Play video when the media source is ready for playback.
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                }
            });
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("chamou","onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.i("chamou","onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("chamou","onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            // Make sure we stop video and release resources when activity is destroyed.
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }

        Log.i("chamou","onDestroy");
    }
}
