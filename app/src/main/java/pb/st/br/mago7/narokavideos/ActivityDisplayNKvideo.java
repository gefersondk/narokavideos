package pb.st.br.mago7.narokavideos;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Geferson on 23/08/16.
 */
public class ActivityDisplayNKvideo extends Activity implements TextureView.SurfaceTextureListener, MediaController.MediaPlayerControl{

    private MediaPlayer mMediaPlayer;

    private MediaController videoController;

    private TextureView mPreview;

    private Context context;

    //@BindView(R.id.progressbar)
    protected ProgressBar mProgressBar;

    private static final String TAG = "ActivityDisplayNKvideo";

    private static final String FILE_NAME = "big_buck_bunny.mp4";

    @Override
    public void onCreate(Bundle icicle) {

        super.onCreate(icicle);
        setContentView(R.layout.surface);
        ButterKnife.bind(this);

        this.context = getApplicationContext();

        initView();
    }

    private void initView() {
        //mProgressBar.setProgress(0);
        //mProgressBar.setMax(100);
        mPreview = (TextureView) findViewById(R.id.surface);
        mPreview.setSurfaceTextureListener(this);
    }
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {

        new MyAsync().execute(surface);
        /*Surface s = new Surface(surface);

        String url = "http://video.beeg.com/data=pc_BR_179.185.143.39_1937/key=wdHZKgbYluFvSzeOgVhJlA%2Cend=1472018893/480p/4942165.mp4";

        try {
            //AssetFileDescriptor afd = getAssets().openFd(FILE_NAME);
            mMediaPlayer = new MediaPlayer();
            //mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            //mMediaPlayer.setDataSource("https://youtu.be/ZutCGMvCocU");
            mMediaPlayer.setDataSource(getApplicationContext(), Uri.parse(url));
            mMediaPlayer.setSurface(s);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepareAsync();



            // Play video when the media source is ready for playback.
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                    Log.i("verificar tempo", "testando");

                    mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                        @Override
                        public void onBufferingUpdate(MediaPlayer mp, int percent) {
                            //Log.i("verificar tempo", percent+"");
                        }
                    });
                    //http://stackoverflow.com/questions/7802645/get-the-progress-time-of-the-video-played-under-videoview
                    mediaPlayer.getCurrentPosition();

                    *//*MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    retriever.setDataSource(mediaPlayer.get);
                    String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    long timeInmillisec = Long.parseLong( time );
                    long duration = timeInmillisec / 1000;
                    long hours = duration / 3600;
                    long minutes = (duration - hours * 3600) / 60;
                    long seconds = duration - (hours * 3600 + minutes * 60);*//*

                    //Log.i("verificar tempo", hours + ":" + minutes + ":" + seconds);
                    Log.i("verificar tempo", mediaPlayer.getDuration() + "asd");
                    //videoController.show();
                }
            });



            *//*videoController = new MediaController(this);
            videoController.setMediaPlayer(this);//your activity which implemented MediaPlayerControl
            //videoController.setAnchorView(mPreview);
            videoController.setMediaPlayer(this);//g
            videoController.setAnchorView(mPreview);
            videoController.setEnabled(true);
            videoController.show();*//*

        } catch (IllegalArgumentException e) {
            Log.d(TAG, e.getMessage());
        } catch (SecurityException e) {
            Log.d(TAG, e.getMessage());
        } catch (IllegalStateException e) {
            Log.d(TAG, e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        }

        mPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mMediaPlayer.stop();
                //videoController.show();
                if(mMediaPlayer.isPlaying())
                    mMediaPlayer.pause();
                else
                    mMediaPlayer.start();

            }
        });*/
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    @Override
    public void start() {
        Toast.makeText(context, "teste", Toast.LENGTH_LONG).show();

    }

    @Override
    public void pause() {
        Toast.makeText(context, "teste", Toast.LENGTH_LONG).show();

    }

    @Override
    public int getDuration() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return 0;
    }

    @Override
    public void seekTo(int pos) {

    }

    @Override
    public boolean isPlaying() {
        return mMediaPlayer.isPlaying();
        //return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return mMediaPlayer.isPlaying();

    }

    @Override
    public boolean canSeekBackward() {
        return false;
    }

    @Override
    public boolean canSeekForward() {
        return false;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    private class MyAsync extends AsyncTask<SurfaceTexture, Integer, Void> {

        int duration = 0;
        int current = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(SurfaceTexture ... params) {

            Surface s = new Surface(params[0]);

            String url = "http://ip37045792.ahcdn.com/key=fDbuR59KVoDZYVw1n0oOhA,end=1472540474,limit=1/data=pc_BR_179.183.186.91_1938/state=s7e6/buffer=3000529:824616,327.0/speed=176502/reftag=20037190/ssd1/23/1/38387121/480p/4472428.mp4";

            try {
                //AssetFileDescriptor afd = getAssets().openFd(FILE_NAME);
                mMediaPlayer = new MediaPlayer();
                //mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                //mMediaPlayer.setDataSource("https://youtu.be/ZutCGMvCocU");
                mMediaPlayer.setDataSource(getApplicationContext(), Uri.parse(url));
                mMediaPlayer.setSurface(s);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepareAsync();


                //mMediaPlayer.start();//TODO teste g
                // Play video when the media source is ready for playback.
                mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(final MediaPlayer mediaPlayer) {
                        mediaPlayer.start();

                        Log.i("verificar tempo", "testando");
                        duration = mediaPlayer.getDuration();
                        mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                            @Override
                            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                                //Log.i("verificar tempo", percent+"");
                            }
                        });
                        //http://stackoverflow.com/questions/7802645/get-the-progress-time-of-the-video-played-under-videoview
                        //mediaPlayer.getCurrentPosition();
                        /*new Thread(new Runnable() {
                            @Override
                            public void run() {

                                do {
                                    current = mMediaPlayer.getCurrentPosition();
                                    //System.out.println("duration - " + duration + " current- "
                                    //+ current);
                                    try {
                                        //publishProgress((int) (current * 100 / duration));
                                        mProgressBar.setProgress((30));
                                        if(mProgressBar.getProgress() >= 100){
                                            break;
                                        }
                                    } catch (Exception e) {
                                    }
                                } while (mProgressBar.getProgress() <= 100);
                            }
                        });*/


                    /*MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    retriever.setDataSource(mediaPlayer.get);
                    String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    long timeInmillisec = Long.parseLong( time );
                    long duration = timeInmillisec / 1000;
                    long hours = duration / 3600;
                    long minutes = (duration - hours * 3600) / 60;
                    long seconds = duration - (hours * 3600 + minutes * 60);*/

                        //Log.i("verificar tempo", hours + ":" + minutes + ":" + seconds);
                        Log.i("verificar tempo", mediaPlayer.getDuration() + "asd");
                        //videoController.show();

                        videoController = new MediaController(ActivityDisplayNKvideo.this);
                        videoController.setMediaPlayer(ActivityDisplayNKvideo.this);//your activity which implemented MediaPlayerControl
                        //videoController.setAnchorView(mPreview);
                        videoController.setMediaPlayer(ActivityDisplayNKvideo.this);//g
                        videoController.setAnchorView(mPreview);
                        videoController.setEnabled(true);
                        videoController.show();
                    }
                });









            } catch (IllegalArgumentException e) {
                Log.d(TAG, e.getMessage());
            } catch (SecurityException e) {
                Log.d(TAG, e.getMessage());
            } catch (IllegalStateException e) {
                Log.d(TAG, e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
            }
            //http://stackoverflow.com/questions/7802645/get-the-progress-time-of-the-video-played-under-videoview
            //https://skydev.icolabora.com.br/#solutions/85c0a991882846e88cc08b8515f15e27/datastreams/e40593841bd642e2b8c76850ee07dc35
            /*do {
                current = mVideoView.getCurrentPosition();
                System.out.println("duration - " + duration + " current- "
                        + current);
                try {
                    publishProgress((int) (current * 100 / duration));
                    if(mProgressBar.getProgress() >= 100){
                        break;
                    }
                } catch (Exception e) {
                }
            } while (mProgressBar.getProgress() <= 100);*/

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            //mProgressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
